package spaceship;

import java.util.List;

public class Spaceship {

    private String shipName;
    private int shieldPercent;
    private int shellPercent;
    private int lifeSupportSystem;
    private int energySuply;
    private int androidsCount;
    private int photonentorpedosCount;
    private List<String> broadcastCommunicator;
    private List<Cargo> cargos;

    /**
     * Der leere Kontruktor vom Raumschiff.
     */
    public Spaceship(){}

    /**
     * Der befüllte Konstruktor vom Raumschiff.
     *
     * @param shipName shipName
     * @param shellPercent shellPercent
     * @param shieldPercent shieldPercent
     * @param lifeSupportSystem lifeSupportSystem
     * @param energySuply energySuply
     * @param androidsCount androidsCount
     * @param photonentorpedosCount photonentorpedosCount
     * @param broadcastCommunicator broadcastCommunicator
     * @param cargos cargos
     */
    public Spaceship(
            String shipName,
            int shellPercent,
            int shieldPercent,
            int lifeSupportSystem,
            int energySuply,
            int androidsCount,
            int photonentorpedosCount,
            List<String> broadcastCommunicator,
            List<Cargo> cargos
    )
    {
        this.shipName = shipName;
        this.shellPercent = shellPercent;
        this.shieldPercent = shieldPercent;
        this.lifeSupportSystem = lifeSupportSystem;
        this.energySuply = energySuply;
        this.androidsCount = androidsCount;
        this.photonentorpedosCount = photonentorpedosCount;
        this.broadcastCommunicator = broadcastCommunicator;
        this.cargos = cargos;
    }

    /**
     * Fügt der Laadung etwas hinzu.
     *
     * @param cargo cargo
     */
    public void addCargo(Cargo cargo)
    {
        this.cargos.add(cargo);
    }

    /**
     * Gibt den Schiffsstatus uns das Ladeverzeichnis auf der Console aus.
     */
    public void getShipState()
    {
        System.out.println(
                "shipName: " + this.shipName +
                        ", \nshellPercent: " + this.shellPercent +
                        ", \nshieldPercent: " + this.shieldPercent +
                        ", \nlifeSupportSystem. " + this.lifeSupportSystem +
                        ", \nenergySuply: " + this.energySuply +
                        ", \nandroidsCount: " + this.androidsCount +
                        ", \nphotonentorpedosCount: " + this.photonentorpedosCount +
                        ", \nbroadcastCommunicator: " + this.broadcastCommunicator
        );
        this.getCargoDirectory();
    }

    /**
     * Gibt das Ladeverzeichnis auf der Console aus.
     */
    public void getCargoDirectory()
    {
        for (Cargo cargo: cargos) {
            System.out.println("Cargo: " + cargo);
        }
    }

    /**
     * Schießt Photonentorpedos aus einen Schiff das übergeben wird.
     * @param spaceship spaceship
     */
    public void torpedosLaunching(Spaceship spaceship)
    {
        if (this.photonentorpedosCount < 1) {
            this.sendMessage("-=*Click*=-");
        }
        else {
            this.photonentorpedosCount--;
            this.sendMessage("Photonentorpedo abgeschossen");
            this.score(spaceship);
        }
    }

    /**
     * Schießt Phaserkannonen auf ein Schiff das Übergeben wird.
     *
     * @param spaceship spaceship
     */
    public void phasercannonLaunching(Spaceship spaceship)
    {
        if (this.energySuply < 50) {
            this.sendMessage("-=*Click*=-");
        }
        else {
            this.energySuply = - 50;
            this.sendMessage("Photonenkanone abgeschossen");
            this.score(spaceship);
        }
    }

    /**
     * Notiert ein Treffer.
     *
     * @param spaceship spaceship
     */
    private void score(Spaceship spaceship)
    {
        this.sendMessage(spaceship.shipName + "wurde getroffen");
        spaceship.shieldPercent -= 50;

        if (spaceship.shieldPercent < 1){
            spaceship.energySuply -= 50;
            spaceship.shellPercent -= 50;

            if (spaceship.shellPercent < 1) {
                spaceship.lifeSupportSystem = 0;
                System.out.println("Die Lebenserhaltungssysteme  von " + spaceship.shipName + " sind erfolgreich zerstört");
                this.sendMessage("Die Lebenserhaltungssysteme  von " + spaceship.shipName + " sind erfolgreich zerstört");
            }
        }
    }

    /**
     * Sendet eine Nachricht in den Broadcastkommunikator und schreibt diese auf die Konsole.
     *
     * @param message message
     */
    public void sendMessage(String message)
    {
        this.broadcastCommunicator.add(message);
        System.out.println(message);
    }

    /**
     * Gibt das Lockbuch zurück.
     */
    public void returnLogbook()
    {
        for (String message: this.broadcastCommunicator) {
            System.out.println("Nachricht: " + message);
        }
    }

    /**
     * Läd die Torpedorohre nach.
     *
     * @param quantity quantity
     */
    public void loadTorpedoTubes(int quantity)
    {
        for (Cargo cargo: this.cargos) {
            if (cargo.getDesignation().equals("Photonentorpedos")) {
                if (cargo.getQuantity() > quantity) {
                    quantity = cargo.getQuantity();
                }

                cargo.setQuantity(cargo.getQuantity() - quantity);
                setPhotonentorpedosCount(quantity);
                System.out.println(quantity + " Photonentorpedos eingesetzte");

                return;
            }
        }

        System.out.println("Keine Photonentorpedos gefunden");
        this.sendMessage("-=*Click*=-");
    }

    /**
     * Benutzt die Androiden um das Schiff zu reaparieren.
     *
     * @param androidRequired androidRequired
     * @param energySupply energySupply
     * @param shield shield
     * @param lifeSupport lifeSupport
     * @param shell shell
     */
    public void useRepairAndroid(int androidRequired, boolean energySupply, boolean shield, boolean lifeSupport, boolean shell) {
        int rnd = (int) (Math.random() * 100);
        int structuresToRepair = 0;
        androidRequired = Math.min(getAndroidsCount(), androidRequired);
        setAndroidsCount(getAndroidsCount() - androidRequired);

        if (energySupply) {
            structuresToRepair++;
        }
        if (shield) {
            structuresToRepair++;
        }
        if (lifeSupport) {
            structuresToRepair++;
        }
        if (shell) {
            structuresToRepair++;
        }

        int repairBy = rnd * androidRequired / structuresToRepair;

        if (energySupply) {
            setEnergySuply(getEnergySuply() + repairBy);
        }
        if (shield) {
            setShieldPercent(getShieldPercent() + repairBy);
        }
        if (lifeSupport) {
            setLifeSupportSystem(getLifeSupportSystem() + repairBy);
        }
        if (shell) {
            setShellPercent(getShellPercent() + repairBy);
        }
    }

    /**
     * leert das Ladeverzeichnis.
     */
    public void clearCargoDirection(){
        this.cargos.removeIf(cargo -> cargo.getQuantity() < 1);
    }

    /**
     * Gibt den Schiffsnamen zurück.
     *
     * @return shipName
     */
    public String getShipName()
    {
        return shipName;
    }

    /**
     * Setzt den Schiffnamen
     *
     * @param shipName shipName
     */
    public void setShipName(String shipName)
    {
        this.shipName = shipName;
    }

    /**
     * Gibt die Schildstärke in Prozent zurück.
     *
     * @return shieldPercent
     */
    public int getShieldPercent()
    {
        return shieldPercent;
    }

    /**
     * Setzte die Schildstärke in Prozent.
     *
     * @param shieldPercent shieldPercent
     */
    public void setShieldPercent(int shieldPercent) {
        this.shieldPercent = shieldPercent;
    }

    /**
     * Gibt die Schiffshülle in Prozent zurück.
     *
     * @return shellPercent
     */
    public int getShellPercent() {
        return shellPercent;
    }

    /**
     * Setzt die Schiffhülle in Prozent.
     *
     * @param shellPercent shellPercent
     */
    public void setShellPercent(int shellPercent) {
        this.shellPercent = shellPercent;
    }

    /**
     * Gibt den Status des Lebenserhaltungssystems in Prozent zurück.
     *
     * @return lifeSupportSystem
     */
    public int getLifeSupportSystem() {
        return lifeSupportSystem;
    }

    /**
     * Setzt den Status des Lebenserhaltungssystems in Prozent.
     *
     * @param lifeSupportSystem lifeSupportSystem
     */
    public void setLifeSupportSystem(int lifeSupportSystem) {
        this.lifeSupportSystem = lifeSupportSystem;
    }

    /**
     * Gibt den Status des Energieversorgung in Prozent zurück.
     *
     * @return energySuply
     */
    public int getEnergySuply() {
        return energySuply;
    }

    /**
     * Setzt den Status des Energieversorgung in Prozent.
     *
     * @param energySuply energySuply
     */
    public void setEnergySuply(int energySuply) {
        this.energySuply = energySuply;
    }

    /**
     * Gibt die Anzahl der Androiden zurück.
     *
     * @return androidsCount
     */
    public int getAndroidsCount() {
        return androidsCount;
    }

    /**
     * Setzt die Anzahl der Androiden.
     *
     * @param androidsCount androidsCount
     */
    public void setAndroidsCount(int androidsCount) {
        this.androidsCount = androidsCount;
    }

    /**
     * Gibt die Anzahl der Photonentorpedos zurück.
     *
     * @return photonentorpedosCount
     */
    public int getPhotonentorpedosCount() {
        return photonentorpedosCount;
    }

    /**
     * Setzt die Anzahl der Photonentorpedos.
     *
     * @param photonentorpedosCount photonentorpedosCount
     */
    public void setPhotonentorpedosCount(int photonentorpedosCount) {
        this.photonentorpedosCount = photonentorpedosCount;
    }

    /**
     * Gibt den Broadcastkommunikator zurück.
     *
     * @return broadcastCommunicator
     */
    public List<String> getBroadcastCommunicator() {
        return broadcastCommunicator;
    }

    /**
     * Setzt den Broadcastkommunikator.
     *
     * @param broadcastCommunicator broadcastCommunicator
     */
    public void setBroadcastCommunicator(List<String> broadcastCommunicator) {
        this.broadcastCommunicator = broadcastCommunicator;
    }

    /**
     * Gibt die Ladung zurück.
     *
     * @return cargos
     */
    public List<Cargo> getCargos() {
        return cargos;
    }

    /**
     * Setzt die Ladung.
     *
     * @param cargos cargos
     */
    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
    }

}
