package spaceship;

public class Cargo {
    private String designation;
    private int quantity;

    /**
     * Der leere Kontruktor von der Ladung.
     */
    public Cargo(){}

    /**
     * Der befüllte Kontruktor von der Ladung.
     *
     * @param designation designation
     * @param quantity quantity
     */
    public Cargo(String designation, int quantity)
    {
        setDesignation(designation);
        setQuantity(quantity);
    }

    /**
     * Gibt die Beschreibung der Ladung zurück.
     *
     * @return designation
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * Setzt die Beschreibung der Ladung.
     *
     * @param designation designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * Gibt die Menge der Ladung zurück.
     *
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Setzt die Liste der Ladung.
     *
     * @param quantity quantity
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Cargo [designation: " + designation + ", quantity: " + quantity + "]";
    }
}
