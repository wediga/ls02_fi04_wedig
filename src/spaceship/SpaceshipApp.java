package spaceship;

import com.sun.tools.javac.util.List;
import java.util.ArrayList;

public class SpaceshipApp {

    public static void main(String[] args) {

        Spaceship klingonen = new Spaceship(
                "IKS Hegh'ta",
                100,
                100,
                100,
                100,
                2,
                1,
                new ArrayList<>(),
                new ArrayList<>(List.of(
                        new Cargo("Bat'leth Klingonen Schwert", 200),
                        new Cargo("Ferengi Schneckensaft", 200))
                )
        );
        Spaceship romulaner = new Spaceship(
                "IKW Khazara",
                100,
                100,
                100,
                100,
                2, 2,
                new ArrayList<>(),
                new ArrayList<>(List.of(new Cargo("Plasma-Waffee", 50),
                        new Cargo("Borg Schrott", 5),
                        new Cargo("Rote Materie", 2)))
        );
        Spaceship vulkanier = new Spaceship(
                "Ni'Var",
                80,
                50,
                100,
                80,
                5,
                0,
                new ArrayList<>(),
                new ArrayList<>(List.of(
                        new Cargo("Photonentorpedos",3),
                        new Cargo("Forschungssonde",35)))
        );

        klingonen.torpedosLaunching(romulaner);
        System.out.println("--------------------------------------");
        romulaner.phasercannonLaunching(klingonen);
        System.out.println("--------------------------------------");
        vulkanier.sendMessage("Gewalt ist nicht logisch");
        System.out.println("--------------------------------------");
        klingonen.getShipState();
        System.out.println("--------------------------------------");
        vulkanier.useRepairAndroid(
                vulkanier.getAndroidsCount(),
                vulkanier.getEnergySuply()<=100,
                vulkanier.getShieldPercent()<=100,
                vulkanier.getLifeSupportSystem()<=100,
                vulkanier.getShellPercent()<=100
        );
        System.out.println("--------------------------------------");
        vulkanier.loadTorpedoTubes(vulkanier.getPhotonentorpedosCount());
        System.out.println("--------------------------------------");
        vulkanier.clearCargoDirection();
        System.out.println("--------------------------------------");
        klingonen.torpedosLaunching(romulaner);
        System.out.println("--------------------------------------");
        klingonen.torpedosLaunching(romulaner);
        System.out.println("--------------------------------------");
        klingonen.getShipState();
        System.out.println("--------------------------------------");
        romulaner.getShipState();
        System.out.println("--------------------------------------");
        vulkanier.getShipState();
    }
}
