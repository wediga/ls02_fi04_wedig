package kreis;

import java.awt.*;

public class Testkreis {
    public static void main(String[] args) {
        Point p = new Point(3, 4);
        Kreis k = new Kreis(50, p);
        System.out.println("Fläche: " + k.getFlaeche());
        System.out.println("Umfang: " + k.getUmfang());
        System.out.println("Durchmesser: " + k.getDurchmesser());

    }
}
