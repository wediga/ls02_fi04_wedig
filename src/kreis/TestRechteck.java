package kreis;

import java.awt.*;

public class TestRechteck {
    public static void main(String[] args) {
        Rechteck k = new Rechteck(5, 10, new Point(3,4));
        System.out.println();
        System.out.println("Diagonale: " + k.getDiagonale());
        System.out.println("Flaeche: " + k.getFlaeche());
        System.out.println("Umfang: " + k.getUmfang());
    }
}
