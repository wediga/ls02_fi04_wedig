package kreis;

import java.awt.*;

public class Rechteck {
    private double hoehe;
    private double breite;
    private Point mittelpunkt;

    public Rechteck(double hoehe, double breite, Point mittelpunkt)
    {
        setHoehe(hoehe);
        setBreite(breite);
    }

    public double getDiagonale()
    {
        return Math.sqrt((this.breite * this.breite) + (this.hoehe * this.hoehe));
    }

    public double getUmfang()
    {
        return (2 * this.hoehe) + (2 * this.breite);
    }

    public double getFlaeche()
    {
        return this.hoehe * this.breite;
    }

    public double getHoehe() {
        return hoehe;
    }

    public void setHoehe(double hoehe) {
        this.hoehe = hoehe;
    }

    public double getBreite() {
        return breite;
    }

    public void setBreite(double breite) {
        this.breite = breite;
    }

    public Point getMittelpunkt() {
        return mittelpunkt;
    }

    public void setMittelpunkt(Point mittelpunkt) {
        this.mittelpunkt = mittelpunkt;
    }
}

